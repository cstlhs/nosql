const service = require('feathers-memory');
const orders = require('../data/orders.json')

module.exports = service({
  store: orders,
  startId: Math.max(...orders.map(o => o.id))+1
})