const service = require('feathers-memory');
var customers = require('../data/customers.json')

module.exports = service({
  store: customers
})